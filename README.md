## Synopsis

Bookmarker is a simple web app that helps you to keep track of the countless books you may find yourself reading during the week. Good for students or professional's even stay home mommy or daddies that like to read alot of books. Bookmarker code is forked from Jerome Gravel-Niquet Todos App. Permission granted by Jerome Gravel-Niquet.

## Code Example

Bookmarker code is developed with backbone.js, underscore.js, jquery and compliant HTML/CSS.

Example code from book.js

    // Book Collection
    // ---------------
    // The collection of books is backed by *localStorage* instead of a remote server.
    var BookList = Backbone.Collection.extend({
        // Reference to this collection's model.
        model: Book,
        // Save all of the book items under the "books-backbone" namespace.
        localStorage: new Backbone.LocalStorage("books-backbone")
    });

## Motivation

While studying up on backbone.js, Bookmarker was developed.

## Installation

Installation is easy. Drag and drop the entire folder into your host root directory. After upload is complete, browse to index.html.

E.g. http://www.yourdomain.com/bookmarker/index.html.

Bookmarker also makes use of localstorage, this means that your data will persist until you decide to clear out your browser's storage.

## Contributors

* www.puffstream.com
* https://twitter.com/jamesstar8

## License

General Public License
